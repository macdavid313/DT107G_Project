;;;; web.lisp
(in-package :dt107g)

(annot:enable-annot-syntax)

@export
(defun start-dt107g
    (&key (port 8080) (datastore 'dt107g.pg-datastore:pg-datastore) (datastore-init nil))
  (progn
    (setf *datastore* (apply #'make-instance datastore datastore-init))
    (init)
    (start 'dt107g :port port)))

@export (defun stop-dt107g () (restas:stop-all))

;;; Static Components(JS, CSS, IMG ...)
(define-route m-xml-js
    ("/static/js/m-xml.js" :content-type "text/javascript")
  (merge-pathnames "m-xml.js" *js-path*))

(define-route mysite-css
    ("/static/css/mysite.css" :content-type "text/css")
  (merge-pathnames "mysite.css" *css-path*))

(define-route index-css
    ("/static/css/index.css" :content-type "text/css")
  (merge-pathnames "index.css" *css-path*))

(define-route sign-in-css
    ("/static/css/sign-in.css" :content-type "text/css")
  (merge-pathnames "sign-in.css" *css-path*))

(define-route blog.css
    ("/static/css/blog.css" :content-type "text/css")
  (merge-pathnames "blog.css" *css-path*))

(define-route single-post-css
    ("/static/css/single-post.css" :content-type "text/css")
  (merge-pathnames "single-post.css" *css-path*))

(define-route admin-css
    ("/static/css/admin.css" :content-type "text/css")
  (merge-pathnames "admin.css" *css-path*))

(define-route lisp-logo-img
    ("/static/img/made-with-lisp-logo.jpg" :content-type "image/jpeg")
  (merge-pathnames "made-with-lisp-logo.jpg" *img-path*))

;;; Web Routes
(define-route index ("/")
  (let ((nickname (current-user-nickname)))
    (if nickname
	(let ((blog-title (getf (find-user nickname) :blog-title)))
	  (web.tmpl:index (list :blog-title blog-title :user nickname :top-hits (prepare-top-hits))))
	(web.tmpl:index (list :top-hits (prepare-top-hits))))))

(define-route register ("/register") (web.tmpl:register))

(define-route handle-register ("/handle-register" :method :post)
  (if
   (and (string= (hunchentoot:post-parameter "password") (hunchentoot:post-parameter "retype_password"))
	(register-user :firstname (hunchentoot:post-parameter "firstname")
		       :lastname (hunchentoot:post-parameter "lastname")
		       :nickname (hunchentoot:post-parameter "nickname")
		       :password (hunchentoot:post-parameter "password")
		       :blog-title (hunchentoot:post-parameter "blog_title")
		       :blog-short-description (hunchentoot:post-parameter "blog_short_description")))
   (log-in (hunchentoot:post-parameter "nickname"))
   (hunchentoot:redirect "http://localhost:8080/register")))

(define-route sign-in ("/sign-in") (web.tmpl:sign-in))

(define-route handle-sign-in ("/handle-sign-in" :method :post)
  (let ((nickname (hunchentoot:post-parameter "nickname"))
	(password (hunchentoot:post-parameter "password")))
    (if (and nickname password
	     (auth-user nickname password))
	(log-in nickname)
	(hunchentoot:redirect "http://localhost:8080/sign-in"))))

(define-route logout ("/logout") (log-out))

(define-route user-blog ("/blog/:(nickname)")
  (let ((plist (find-user nickname)))
    (if (not plist)
	(hunchentoot:redirect "http://localhost:8080")
	(let ((blog-title (getf plist :blog-title))
	      (blog-short-description (getf plist :blog-short-description)))
	  (web.tmpl:blog-page (list :posts (prepare-post-list nickname)
				    :user (current-user-nickname)
				    :blog-title blog-title
				    :blog-short-description blog-short-description))))))

(define-route single-post ("/blog/:(nickname)/:(url)")
  (let ((plist (find-user nickname)))
    (if (not plist)
	(hunchentoot:redirect "http://localhost:8080")
	(let ((timestamp (fetch-timestamp url)))
	  (web.tmpl:single-post
	   (append (prepare-single-post nickname timestamp)
		   (list :user (current-user-nickname))))))))

(define-route admin ("/admin/:(nickname)")
  (let ((plist (find-user nickname)))
    (if (not plist) 
	(hunchentoot:redirect "http://localhost:8080")
	(if (user-logged-on-p nickname)
	    (web.tmpl:admin (append (list :posts (prepare-admin-page nickname)) (list :user nickname)))
	    (hunchentoot:redirect "http://localhost:8080/sign-in")))))

(define-route admin-create ("/admin/:(nickname)/create-post")
  (if (not (find-user nickname))
      (hunchentoot:redirect "http://localhost:8080")
      (if (user-logged-on-p nickname)
	  (web.tmpl:admin-create (list :user nickname))
	  (hunchentoot:redirect "http://localhost:8080/sign-in"))))

(define-route admin-update ("/admin/update/:(nickname)/:(url)")
  (if (not (find-user nickname))
      (hunchentoot:redirect "http://localhost:8080")
      (if (user-logged-on-p nickname)
	  (let ((timestamp (fetch-timestamp url)))
	    (web.tmpl:admin-update (prepare-update nickname timestamp)))
	  (hunchentoot:redirect "http://localhost:8080/sign-in"))))

(define-route handle-create ("/admin/:(nickname)/handle-create-post" :method :post)
  (if (not (find-user nickname))
      (hunchentoot:redirect "http://localhost:8080")
      (if (not (user-logged-on-p nickname))
	  (hunchentoot:redirect "http://localhost:8080/sign-in")
	  (let ((title (hunchentoot:post-parameter "title"))
		(content-md (hunchentoot:post-parameter "content_md")))
	    (progn
	      (insert-post :user-nickname nickname :title title :content-md content-md)
	      (hunchentoot:redirect (concatenate 'string "http://localhost:8080/admin/" nickname)))))))

(define-route handle-update ("/admin/:(nickname)/handle-update-post" :method :post)
  (if (not (find-user nickname))
      (hunchentoot:redirect "http://localhost:8080")
      (if (not (user-logged-on-p nickname))
	  (hunchentoot:redirect "http://localhost:8080/sign-in")
	  (let ((timestamp (hunchentoot:post-parameter "timestamp"))
		(title (hunchentoot:post-parameter "title"))
		(content-md (hunchentoot:post-parameter "content_md")))
	    (progn
	      (update-post :user-nickname nickname
			   :timestamp timestamp
			   :new-title title
			   :new-content-md content-md)
	      (hunchentoot:redirect (concatenate 'string "http://localhost:8080/admin/" nickname)))))))

(define-route handle-delete ("/admin/delete/:(nickname)/:(url)")
  (if (not (find-user nickname))
      (hunchentoot:redirect "http://localhost:8080")
      (if (not (user-logged-on-p nickname))
	  (hunchentoot:redirect "http://localhost:8080/sign-in")
	  (let ((timestamp (fetch-timestamp url)))
	    (progn
	      (delete-post :user-nickname nickname :timestamp timestamp)
	      (hunchentoot:redirect (concatenate 'string "http://localhost:8080/admin/" nickname)))))))

(define-route user-list ("/admin/user-list") (prepare-user-list))

(define-route rss ("/admin/:(nickname)/export-rss")
  (if (not (find-user nickname))
      (hunchentoot:redirect "http://localhost:8080")
      (if (not (user-logged-on-p nickname))
	  (hunchentoot:redirect "http://localhost:8080/sign-in")
	  (xml.tmpl:rss (list :user nickname
			      :blog-short-description (getf (find-user nickname) :blog-short-description)
			      :posts (prepare-rss nickname))))))
