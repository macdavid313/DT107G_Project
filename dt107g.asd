(in-package :cl-user)
(defpackage dt107g.asd
  (:use :cl))
(in-package :dt107g.asd)

(asdf:defsystem dt107g
  :defsystem-depends-on (:closure-template)
  :depends-on (;; Web
	       :restas
	       :closure-template
	       ;; Database And XML
	       :postmodern
	       :cl-libxml2
	       :cl-libxslt
	       ;; Utilities
	       :split-sequence
	       :cl-ppcre
	       :cl-annot
	       ;; Utilities for Markdown
	       :3bmd
	       :3bmd-ext-wiki-links
	       :3bmd-ext-definition-lists
	       :3bmd-ext-tables)
  :serial t
  :components ((:module "static/templates"
			:components
			((:closure-template "default")
			 (:closure-template "index")
			 (:closure-template "register")
			 (:closure-template "blog")
			 (:closure-template "admin")
			 (:closure-template "xml")))
	       (:file "defmodule")
	       (:file "utils")
	       (:file "pg-datastore")
	       (:file "sedna-datastore")
	       (:file "web")))
