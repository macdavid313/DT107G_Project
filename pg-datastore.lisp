;;;; pg-datastore.lisp
(in-package :cl-user)

(defpackage dt107g.pg-datastore
  (:use :cl
	:postmodern
	:dt107g.policy.datastore
	:split-sequence
	:cl-ppcre)
  (:nicknames :pg-datastore)
  (:export :pg-datastore)
  (:import-from :dt107g
		:format-date
		:markdown-content
		:genurl))

(in-package :dt107g.pg-datastore)

(annot:enable-annot-syntax)

(defclass pg-datastore ()
  ((connection-spec :initarg :connection-spec
		    :initform (list "dt107g_db" "dt107g_admin" "macdavid" "localhost")
                    :reader connection-spec)))

(defparameter *pg-datastore* ;; only for debug
  (make-instance 'pg-datastore
		 :connection-spec (list "dt107g_db" "dt107g_admin" "macdavid" "localhost")))

;;; Utils
;; Notice that when using defprepared, it must be in a with-connection context.
(defprepared the-user-id
    (:select 'id :from 'user
	     :where (:= 'nickname '$1))
  :single)

(defprepared the-post-id
    (:select 'id :from 'post
	     :where (:and (:= 'owner-id '$1)
			  (:= 'timestamp '$2)))
  :single)

(defprepared del-post
    (:delete-from 'post
		  :where (:and (:= 'owner-id '$1)
			       (:= 'timestamp '$2))))

(defprepared count-posts%
    (:select (:count 'id) :from 'post
	     :where (:= 'owner-id '$1))
  :single)

;;; Draft for validating a user-model(Not implemented).
;;; They are all predicates.
(defun validate-name (name)
  (let ((len (length name)))
    (and (>= len 1)
	 (<= len 20)
	 (let ((result (scan-to-strings "[a-zA-Z]+" name)))
	   (and result (string= name result))))))

(defun validate-password (password)
  (let ((len (length password)))
    (and (>= len 6)
	 (<= len 16)
	 (let ((result (scan-to-strings "[A-Z][0-9a-zA-Z!@#$%^*_]+" password)))
	   (and result (string= password result))))))

(defun validate-blog-title (blog-title)
  (let ((len (length blog-title)))
    (and (>= len 1) (<= len 50))))

(defun validate-blog-short-description (blog-short-description)
  (let ((len (length blog-short-description)))
    (and (>= len 0) (<= len 100))))

;;; Models(DAOs)
(defclass user ()
  ((id :col-type serial
       :reader user-id)
   (firstname :col-type string
	      :accessor firstname
	      :initarg :firstname)
   (lastname :col-type string
	     :accessor lastname
	     :initarg :lastname)
   (password :col-type string
	     :accessor password
	     :initarg :password)
   (nickname :col-type string
	     :accessor nickname
	     :initarg :nickname)
   (blog-title :col-type string
	       :accessor user-blog-title
	       :initarg :blog-title)
   (blog-short-description :col-type string
			   :accessor user-blog-short-description
			   :initarg :blog-short-description))
  (:documentation "DAO definition for user.")
  (:metaclass dao-class)
  (:keys id))

(defclass post ()
  ((id :col-type serial
       :reader post-id)
   (owner-id :col-type int
	     :reader owner-id
	     :initarg :owner-id)
   (title :col-type string
	  :initform ""
	  :initarg :post-title
	  :accessor post-title)
   (content-md :col-type text
	       :initform ""
	       :initarg :post-content-md
	       :accessor post-content-md)
   (content-html :col-type text
		 :initform ""
		 :initarg :post-content-html
		 :accessor post-content-html)
   (timestamp :col-type string
	      :initarg :timestamp
	      :reader post-timestamp)
   (create-time :col-type string
		:initform ""
		:initarg :post-create-time
		:reader post-create-time)
   (revised-time :col-type string
		 :initform ""
		 :accessor post-revised-time)
   (url :col-type string
	:initarg :post-url
	:accessor post-url))
  (:documentation "DAO definition for post.")
  (:metaclass dao-class)
  (:keys id))

(deftable post
  (!dao-def)
  (!foreign 'user 'owner-id 'id))

;;; Methods
(defmethod datastore-init ((datastore pg-datastore))
  (with-connection (connection-spec datastore)
    (progn
      (unless (table-exists-p "user")
	(execute (dao-table-definition 'user)))
      (unless (table-exists-p 'post)
	(execute (dao-table-definition 'post))))))

(defmethod datastore-register-user
    ((datastore pg-datastore) &key firstname lastname nickname password blog-title blog-short-description)
  (and (not (datastore-find-user datastore nickname))
       (validate-name firstname)
       (validate-name lastname)
       (validate-password password)
       (validate-blog-title blog-title)
       (validate-blog-short-description blog-short-description)
       (let ((dao (make-instance 'user
				 :firstname firstname
				 :lastname lastname
				 :nickname nickname
				 :password password
				 :blog-title blog-title
				 :blog-short-description blog-short-description)))
	 (with-connection (connection-spec datastore) (save-dao dao)))))

(defmethod datastore-find-user ((datastore pg-datastore) nickname)
  (with-connection (connection-spec datastore)
    (query (:select :* :from 'user
		    :where (:= 'nickname nickname))
	   :plist)))

(defmethod datastore-auth-user ((datastore pg-datastore) nickname password)
  ;; Stupid Auth :(
  (with-connection (connection-spec datastore)
    (let ((user-plist (datastore-find-user datastore nickname)))
      (when user-plist (string= (getf user-plist :password) password)))))

(defmethod datastore-insert-post ((datastore pg-datastore) &key user-nickname title content-md)
  (with-connection (connection-spec datastore)
    (let ((date (format-date))
	  (owner-id (the-user-id user-nickname))
	  (timestamp (get-universal-time)))
      (when owner-id
	(insert-dao (make-instance 'post
				   :owner-id owner-id
				   :post-create-time date
				   :post-content-md content-md
				   :post-content-html (markdown-content content-md)
				   :post-title title
				   :timestamp timestamp
				   :post-url (genurl date title timestamp)))))))

(defmethod datastore-update-post
    ((datastore pg-datastore) &key user-nickname timestamp new-title new-content-md)
  (with-connection (connection-spec datastore)
    (let* ((owner-id (the-user-id user-nickname))
	   (post-id (the-post-id owner-id timestamp))
	   (post-obj (get-dao 'post post-id)))
      (when post-obj
	(let* ((title (if new-title new-title (post-title post-obj)))
	       (content-md (if new-content-md new-content-md (post-content-md post-obj)))
	       (content-html (if new-content-md (markdown-content content-md) (post-content-html post-obj)))
	       (revised-time (format-date)))
	  (query (:update 'post
			  :set
			  'title '$1
			  'content-md '$2
			  'content-html '$3
			  'revised-time '$4
			  'url '$5
			  :where (:= 'id post-id))
		 title content-md content-html revised-time
		 (genurl (post-create-time post-obj) title timestamp)))))))

(defmethod datastore-delete-post
    ((datastore pg-datastore) &key user-nickname timestamp)
  (with-connection (connection-spec datastore)
    (del-post (the-user-id user-nickname) timestamp)))

;;; To be implemented
;;; prepare single post
;;; prepare all posts
(defmethod datastore-count-posts ((datastore pg-datastore) user-nickname)
  (with-connection (connection-spec datastore)
    (when (datastore-find-user datastore)
      (count-posts% (the-user-id user-nickname)))))

(defmethod datastore-prepare-top-hits ((datastore pg-datastore))
  (with-connection (connection-spec datastore)
    (query (:limit
	    (:order-by (:select 'nickname 'blog-title 'blog-short-description :from 'user) (:random)) '$1)
	   3 :plists)))

(defmethod datastore-prepare-post-list ((datastore pg-datastore) nickname)
  (with-connection (connection-spec datastore)
    (let ((plists
	   (query (:order-by (:select 'title 'url :from 'post
				      :where (:= 'owner-id (the-user-id nickname))) 'id) :plists)))
      (mapcar #'(lambda (x) (push nickname x) (push :owner x)) plists))))
	      
(defmethod datastore-prepare-single-post ((datastore pg-datastore) nickname timestamp)
  (with-connection (connection-spec datastore)
    (let ((post-id (the-post-id (the-user-id nickname) timestamp)))
      (query (:select 'title 'content-html :from 'post :where (:= 'id post-id)) :plist))))

(defmethod datastore-prepare-admin-page ((datastore pg-datastore) nickname)
  (with-connection (connection-spec datastore)
    (query (:order-by (:select 'title 'url :from 'post
			      :where (:= 'owner-id (the-user-id nickname))) 'id)
	   :plists)))

(defmethod datastore-prepare-update ((datastore pg-datastore) nickname timestamp)
  (with-connection (connection-spec datastore)
    (let ((id (the-post-id (the-user-id nickname) timestamp)))
      (append (list :user nickname :timestamp timestamp)
	      (query (:select 'title 'content-md :from 'post
			      :where (:= 'id id))
		     :plist)))))

(defmethod datastore-prepare-user-list ((datastore pg-datastore))
  (with-connection (connection-spec datastore)
    (let* ((lists (query (:select 'nickname :from 'user) :lists))
	  (users (loop for item in lists collect (car item))))
      (web.tmpl:user-list
       (list :users
	     (mapcar #'(lambda (x)
			 (let* ((plist (datastore-find-user datastore x))
				(firstname (getf plist :firstname))
				(lastname (getf plist :lastname))
				(blog-title (getf plist :blog-title))
				(amount (count-posts% (the-user-id x))))
			   (list :firstname firstname :lastname lastname :blog-title blog-title :amount amount)))
		     users))))))

(defmethod datastore-prepare-rss ((datastore pg-datastore) nickname)
  (with-connection (connection-spec datastore)
    (query (:select 'title 'url 'create-time :from 'post
		    :where (:= 'owner-id (the-user-id nickname)))
	   :plists)))

