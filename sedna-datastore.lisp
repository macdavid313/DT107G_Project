;;;; sedna-datastore.lisp
(in-package :cl-user)

(defpackage :dt107g.sedna-datastore
  (:use :cl :dt107g.policy.datastore :iter)
  (:nicknames :sedna-datastore)
  (:export :sedna-datastore)
  (:import-from :dt107g
		:format-date
		:markdown-content
		:genurl
		:get-xml-string
		:write-xml-to-file
		:xml-validate-p
		*blog-xml-path*
		*register-xml-path*
		*register-xsd-path*
		*register-xsl-path*))

(in-package :dt107g.sedna-datastore)

(defclass sedna-datastore () nil)

(defparameter *dbname* "dt107g_db")
(defparameter *collection* "blog")
(defparameter *sedna-datastore* (make-instance 'sedna-datastore))

;;; Utils
(defmacro & (&rest strings) `(concatenate 'string ,@strings))

(defmacro wrap-with (string s) `(& ,s ,string ,s))

(defmacro with-squote (string) `(wrap-with ,string "'"))

(defun execute (query dbname &key (output *standard-output*))
  (ccl:run-program "se_term"
		   (list "-query" query dbname)
		   :output output))

(defparameter *mtx-lock* (bt:make-recursive-lock))

(defmacro with-lock (&body body)
  `(bt:with-recursive-lock-held (*mtx-lock*)
     ,@body))

(defun init-user-blog-db (nickname)
  (let ((original-doc-string (get-xml-string *blog-xml-path*)))
    (xtree:with-parse-document (doc original-doc-string)
      (progn
	(setf (xtree:attribute-value (xtree:root doc) "owner") nickname)
	(xtree:serialize doc (parse-namestring "/tmp/dt107g_db_blog_cache.xml") :pretty-print t)
	(execute (& "LOAD '/tmp/dt107g_db_blog_cache.xml' '" nickname "' '" *collection* "'") *dbname*)))))

(defun post-exists-p (nickname timestamp)
  (let ((stream (make-string-output-stream)))
    (progn
      (execute (& "for \$x in doc("
		  (with-squote nickname) ", " (with-squote *collection*) ")/Blog/post "
		  "where \$x/@timestamp=" (with-squote timestamp)
		  " return \$x/@timestamp")
	       *dbname*
	       :output stream)
      (> (length (get-output-stream-string stream)) 1))))

(defun count-posts (nickname)
  (let ((stream (make-string-output-stream)))
    (progn
      (execute (& "count(doc(" (with-squote nickname) ", " (with-squote *collection*) ")/Blog/post)")
	       *dbname* :output stream)
      (parse-integer (get-output-stream-string stream)))))

(defun fetch-post-values (nickname timestamp)
  (flet ((helper ()
	   (let ((stream (make-string-output-stream)))
	     (progn
	       (execute (& "for \$x in doc("
			   (with-squote nickname) ", " (with-squote *collection*) ")/Blog/post "
			   "where \$x/@timestamp=" (with-squote timestamp)
			   " return \$x")
			*dbname*
			:output stream)
	       (get-output-stream-string stream)))))
    (xtree:with-parse-document (doc (helper))
      (let ((title (xpath:find-string doc "/post/@title"))
	    (create-time (xpath:find-string doc "/post/create_time"))
	    (revised-time (xpath:find-string doc "/post/revised_time"))
	    (content-md (xpath:find-string doc "/post/content_md"))
	    (url (xpath:find-string doc "/post/url")))
	(values title create-time revised-time content-md url)))))

(defun collect-nickname ()
  (xtree:with-parse-document (doc *register-xml-path*)
    (iter (for node in-child-nodes (xtree:root doc) with (:type :xml-element-node))
	  (collect (xtree:attribute-value node "nickname")))))

(defun collect-timestamps (nickname)
  (let ((stream (make-string-output-stream)))
    (progn
      (execute (& "doc(" (with-squote nickname) ", " (with-squote *collection*) ")")
	       *dbname* :output stream)
      (xtree:with-parse-document (doc (get-output-stream-string stream))
	(iter (for node in-child-nodes (xtree:root doc) with (:type :xml-element-node))
	      (collect (xtree:attribute-value node "timestamp")))))))

(defun get-blog-xml-doc (nickname)
  (let ((stream (make-string-output-stream)))
    (progn
      (execute (& "doc(" (with-squote nickname) ", " (with-squote *collection*) ")")
	       *dbname* :output stream)
      (get-output-stream-string stream))))

(defun replace-all (string part replacement &key (test #'char=))
  "Returns a new string in which all the occurences of the part 
is replaced with replacement."
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 1)))
  (with-output-to-string (out)
    (loop with part-length = (length part)
       for old-pos = 0 then (+ pos part-length)
       for pos = (search part string
			 :start2 old-pos
			 :test test)
       do (write-string string out
			:start old-pos
			:end (or pos (length string)))
       when pos do (write-string replacement out)
       while pos)))

;;; Structures used for representing a model
(defstruct user
  (nickname "" :type string)
  (firstname "" :type string)
  (lastname "" :type string)
  (password "" :type string)
  (blog-title "" :type string)
  (blog-short-description "" :type string))

(defstruct post
  (owner-nickname "" :type string)
  (title "" :type string)
  (content-md "" :type string)
  (content-html "" :type string)
  (timestamp (write-to-string (get-universal-time)) :type string)
  (create-time (format-date) :type string)
  (revised-time "" :type string)
  (url "" :type string))

;;; Methods
(defmethod datastore-init ((datastore sedna-datastore)))

(defmethod datastore-find-user ((datastore sedna-datastore) nickname)
  (declare (ignore datastore))
  (with-lock
    (xtree:with-parse-document (doc (get-xml-string *register-xml-path*))
      (when (xpath:find-single-node doc (& "/Register/Profile[@nickname=\"" nickname "\"]"))
	(let ((query-firstname (& "/Register/Profile[@nickname=\"" nickname "\"]/firstname"))
	      (query-lastname (& "/Register/Profile[@nickname=\"" nickname "\"]/lastname"))
	      (query-password (& "/Register/Profile[@nickname=\"" nickname "\"]/password"))
	      (query-blog-title (& "/Register/Profile[@nickname=\"" nickname "\"]/blog_title"))
	      (query-blog-short-description (& "/Register/Profile[@nickname=\"" nickname "\"]/blog_short_description"))
	      (result (list :nickname nickname)))
	  (progn
	    (setf (getf result :firstname) (xpath:find-string doc query-firstname))
	    (setf (getf result :lastname) (xpath:find-string doc query-lastname))
	    (setf (getf result :password) (xpath:find-string doc query-password))
	    (setf (getf result :blog-title) (xpath:find-string doc query-blog-title))
	    (setf (getf result :blog-short-description) (xpath:find-string doc query-blog-short-description))
	    result))))))

(defmethod datastore-register-user
    ((datastore sedna-datastore) &key firstname lastname nickname password blog-title blog-short-description)
  #|
  "Add a profile element to register.xml.
  1. Verify whether the nickname is unique;
  2. Write XML file and validate it using register.xsd;
  3. If there's error, 'rollback' register.xml."
  |#
  (when (not (datastore-find-user datastore nickname))
    (with-lock
      (let ((original-xml-string (get-xml-string *register-xml-path*)))
	(xtree:with-parse-document (doc original-xml-string)
	  (let ((profile% (xtree:make-element "Profile"))
		(firstname% (xtree:make-element "firstname"))
		(lastname% (xtree:make-element "lastname"))
		(password% (xtree:make-element "password"))
		(blog-title% (xtree:make-element "blog_title"))
		(blog-short-description% (xtree:make-element "blog_short_description")))
	    (progn
	      (setf (xtree:attribute-value profile% "nickname") nickname)
	      (xtree:append-child profile% (progn (setf (xtree:text-content firstname%) firstname) firstname%))
	      (xtree:append-child profile% (progn (setf (xtree:text-content lastname%) lastname) lastname%))
	      (xtree:append-child profile% (progn (setf (xtree:text-content password%) password) password%))
	      (xtree:append-child profile% (progn (setf (xtree:text-content blog-title%) blog-title) blog-title%))
	      (xtree:append-child profile% (progn (setf (xtree:text-content blog-short-description%) blog-short-description) blog-short-description%))
	      (xtree:append-child (xtree:root doc) profile%)
	      (xtree:serialize doc *register-xml-path* :pretty-print t))))
	(if (xml-validate-p *register-xml-path* *register-xsd-path*)
	    (progn
	      (init-user-blog-db nickname)
	      't)
	    (progn
	      (write-xml-to-file original-xml-string *register-xml-path*)
	      nil))))))

(defmethod datastore-auth-user ((datastore sedna-datastore) nickname password)
  (with-lock
    (let ((user-plist (datastore-find-user datastore nickname)))
      (when user-plist (string= password (getf user-plist :password))))))

(defmethod datastore-insert-post ((datastore sedna-datastore) &key user-nickname title content-md)
  (when (datastore-find-user datastore user-nickname)
    (let* ((nickname user-nickname)
	   (timestamp (get-universal-time))
	   (create-time (format-date))
	   (revised-time "")
	   (url (genurl create-time title timestamp))
	   (xml-string (xml.tmpl:post (list :title title
					    :timestamp timestamp
					    :create-time create-time
					    :revised-time revised-time
					    :content-md content-md
					    :url url))))
      (execute (& "UPDATE insert " xml-string " into "
		  "doc(" (with-squote nickname) ", " (with-squote *collection*) ")/Blog")
	       *dbname*))))

(defmethod datastore-update-post
    ((datastore sedna-datastore) &key user-nickname timestamp new-title new-content-md)
  (when (and (datastore-find-user datastore user-nickname)
	     (post-exists-p user-nickname timestamp))
    (multiple-value-bind
	  (old-title create-time revised-time old-content-md url) (fetch-post-values user-nickname timestamp)
      (declare (ignore revised-time url))
      (let* ((title (if new-title new-title old-title))
	     (content-md (if new-content-md new-content-md old-content-md))
	     (revised-time (format-date))
	     (url (genurl create-time title timestamp)))
	(execute (& "UPDATE replace $p in doc("
		    (with-squote user-nickname) ", " (with-squote *collection*)
		    ")/Blog/post[@timestamp=" (with-squote timestamp) "] with "
		    (xml.tmpl:post (list :title title
					 :timestamp timestamp
					 :create-time create-time
					 :revised-time revised-time
					 :content-md content-md
					 :url url)))
		 *dbname*)))))

(defmethod datastore-delete-post ((datastore sedna-datastore) &key user-nickname timestamp)
  (when (and (datastore-find-user datastore user-nickname)
	     (post-exists-p user-nickname timestamp))
    (execute (& "UPDATE delete doc("
		(with-squote user-nickname) ", " (with-squote *collection*)
		")/Blog/post[@timestamp=" (with-squote timestamp) "]")
	     *dbname*)))

(defmethod datastore-prepare-top-hits ((datastore sedna-datastore))
  (with-lock
    (xtree:with-parse-document (doc (get-xml-string *register-xml-path*))
      (let ((nicknames (collect-nickname))
	    (result))
	(dolist (name nicknames)
	  (let* ((plist (datastore-find-user datastore name))
		 (title (getf plist :blog-title))
		 (description (getf plist :blog-short-description)))
	    (push (list :nickname name :blog-title title :blog-short-description description) result)))
	(if (> (length result) 3)
	    (list (car result) (cadr result) (caddr result))
	    result)))))

(defmethod datastore-prepare-post-list ((datastore sedna-datastore) nickname)
  (declare (ignore datastore))
  (let (result)
    (dolist (ts (collect-timestamps nickname))
      (multiple-value-bind
	    (title create-time revised-time content-md url) (fetch-post-values nickname ts)
	(declare (create-time revised-time content-md))
	(push (list :owner nickname :title title :url url) result)))
    result))

(defmethod datastore-prepare-single-post ((datastore sedna-datastore) nickname timestamp)
  (declare (ignore datastore))
  (multiple-value-bind
	(title create-time revised-time content-md url) (fetch-post-values nickname timestamp)
    (declare (create-time revised-time url))
    (list :title title :content-html (markdown-content content-md))))

(defmethod datastore-prepare-admin-page ((datastore sedna-datastore) nickname)
  (declare (ignore datastore))
  (let (result)
    (dolist (ts (collect-timestamps nickname))
      (multiple-value-bind
	    (title create-time revised-time content-md url) (fetch-post-values nickname ts)
	(declare (create-time revised-time content-md))
	(push (list :title title :url url) result)))
    result))

(defmethod datastore-prepare-update ((datastore sedna-datastore) nickname timestamp)
  (declare (ignore datastore))
  (multiple-value-bind
	(title create-time revised-time content-md url) (fetch-post-values nickname timestamp)
    (declare (create-time revised-time url))
    (list :user nickname :timestamp timestamp :title title :content-md content-md)))

(defmethod datastore-prepare-user-list ((datastore sedna-datastore))
  (with-lock
    (xslt:with-stylesheet (style (get-xml-string *register-xsl-path*))
      (xtree:with-parse-document (doc (get-xml-string *register-xml-path*))
	(xslt:with-transform-result (res (style doc))
	  (xtree:serialize res :to-string))))))

(defmethod datastore-prepare-rss ((datastore sedna-datastore) nickname)
  (declare (ignore datastore))
  (let (result)
    (dolist (ts (collect-timestamps nickname))
      (multiple-value-bind
	    (title create-time revised-time content-md url) (fetch-post-values nickname ts)
	(declare (create-time content-md))
	(push (list :title title :url url :create-time create-time) result)))
    result))
