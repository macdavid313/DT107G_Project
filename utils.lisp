;;;; utils.lisp
(in-package :dt107g)

(annot:enable-annot-syntax)

@export
(defun format-date ()
  (multiple-value-bind 
	(second minute hour date month year) (get-decoded-time)
    (declare (ignore hour minute second))
    (format nil "~A-~A-~A" year month date)))

@export
(defmacro markdown-content (content)
  (declare (type string content))
  `(with-output-to-string (str)
     (3bmd:parse-string-and-print-to-stream ,content str)))

@export
(defun genurl (date title timestamp)
  (let ((vector (split-sequence #\Space title :remove-empty-subseqs t)))
    (format nil "~a-~a-~a"
	    date
	    (reduce #'(lambda (x y)
			(concatenate 'string x "-" y))
		    vector)
	    timestamp)))

@export
(defun fetch-timestamp (url) (car (last (split-sequence #\- url))))

@export
(defmethod get-xml-string ((xml-document string))
  "Get XML string from a .xml file."
  (let ((result (make-array 0
			    :element-type 'character
			    :adjustable t
			    :fill-pointer 0)))
    (with-open-file (xml xml-document)
      (do ((char (read-char xml nil)
		 (read-char xml nil)))
	  ((null char) (values result (length result)))
	(vector-push-extend char result)))))

@export
(defmethod get-xml-string ((xml-document pathname))
  "Get XML string from a .xml file."
  (get-xml-string (namestring xml-document)))

@export
(defmethod write-xml-to-file (xml (file string))
  (xtree:with-parse-document (doc xml)
    (xtree:serialize doc (parse-namestring file) :pretty-print t)))

@export
(defmethod write-xml-to-file (xml (file pathname))
  (xtree:with-parse-document (doc xml)
    (xtree:serialize doc file :pretty-print t)))

(defmethod validate-xml ((xml string) (xsd string))
  (let ((stream (make-string-output-stream)))
    (ccl:run-program
     "xmllint" ; Using xmllint to validate
     (list "--noout" "--schema" xsd xml) ; arguments
     :output stream)
    (get-output-stream-string stream)))

(defmethod validate-xml ((xml pathname) (xsd pathname))
  (validate-xml (namestring xml) (namestring xsd)))

@export
(defun xml-validate-p (xml-filename xsd-filename)
  (let* ((output (validate-xml xml-filename xsd-filename))
	 (scanner (cl-ppcre:create-scanner "validates$"))
	 (scan (cl-ppcre:scan-to-strings scanner output)))
    (string= scan "validates")))

;;; Web
(defun user-logged-on-p (nickname) (string= nickname (hunchentoot:session-value :nickname)))

(defun current-user-nickname () (hunchentoot:session-value :nickname))

(defun log-in (nickname &optional (redirect-route "http://localhost:8080/"))
  (hunchentoot:start-session)
  (setf (hunchentoot:session-value :nickname) nickname)
  (hunchentoot:redirect redirect-route))

(defun log-out (&optional (redirect-route "http://localhost:8080/"))
  (setf (hunchentoot:session-value :nickname) nil)
  (hunchentoot:redirect redirect-route))

