<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
    <h2>All Users List</h2>
    <table border="1">
    <tr bgcolor="#9acd32">
      <th align="left">Firstname</th>
      <th align="left">Lastname</th>
      <th align="left">Blog Title</th>
    </tr>
    <xsl:for-each select="Register/Profile">
    <tr>
      <td><xsl:value-of select="firstname"/></td>
      <td><xsl:value-of select="lastname"/></td>
      <td><xsl:value-of select="blog_title"/></td>
    </tr>
    </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>
