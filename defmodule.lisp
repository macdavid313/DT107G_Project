;;;; defmodule.lisp
(in-package :cl-user)

(restas:define-policy #:datastore
  (:interface-package #:dt107g.policy.datastore)
  (:interface-method-template "DATASTORE-~A")
  (:internal-function-template "~A")
  (:internal-package #:dt107g.datastore)

  (define-method init ()
    "Initialize the database.")

  (define-method register-user
      (&key firstname lastname nickname password blog-title blog-short-description)
    "Register a user.")
  
  (define-method find-user (nickname)
    "Find user by the nickname. Return a plist.")

  (define-method auth-user (nickname password)
    "Authentication.")

  (define-method insert-post (&key user-nickname title content-md)
    "Given a user's nickname, insert a post into database.")

  (define-method update-post (&key user-nickname timestamp new-title new-content-md)
    "Given a user's nickname, original post's title and its timestamp,
update that post according to the arguments.")

  (define-method delete-post (&key user-nickname timestamp)
    "Given a user's nickname, the post's title and its timestamp,
delete that post from database.")

  (define-method count-posts (user-nickname) "Count posts of a user.")

  (define-method prepare-top-hits () "Prepare top hits for index page.")

  (define-method prepare-post-list (nickname) "Prepare a list of posts of a user.")

  (define-method prepare-single-post (nickname timestamp) "Prepare signle post page.")

  (define-method prepare-admin-page (nickname) "Prepare the admin page for a user.")

  (define-method prepare-update (nickname timestamp) "Prepare to update a post.")

  (define-method prepare-user-list () "Prepare a list of users under admin page.")

  (define-method prepare-rss (nickname) "Prepare a rss for user."))

(restas:define-module dt107g
  (:use :cl
	:split-sequence
	:restas
	:dt107g.datastore))

(in-package :dt107g)

(annot:enable-annot-syntax)

;;; Turn on 'debug mode'
(eval-when
    (:compile-toplevel :load-toplevel :execute)
  (restas:debug-mode-on))

@export (defparameter *root-path* (asdf:system-source-directory 'dt107g))

@export (defparameter *static-path* (merge-pathnames "static/" *root-path*))

@export (defparameter *js-path* (merge-pathnames "js/" *static-path*))

@export (defparameter *css-path* (merge-pathnames "css/" *static-path*))

@export (defparameter *img-path* (merge-pathnames "img/" *static-path*))

@export (defparameter *xml-path* (merge-pathnames "xml/" *static-path*))

@export (defparameter *register-xml-path* (merge-pathnames "register.xml" *xml-path*))

@export (defparameter *register-xsd-path* (merge-pathnames "register.xsd" *xml-path*))

@export (defparameter *register-xsl-path* (merge-pathnames "register.xsl" *xml-path*))

@export (defparameter *blog-xml-path* (merge-pathnames "blog.xml" *xml-path*))

